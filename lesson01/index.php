<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>PHP Lesson</title>
</head>
<body>
  <h1>Q1</h1>
  <h2>ニンテンドースイッチ版ドラゴンクエストXI</h2>

  <dl>
  <dt>発売元：</dt>
  <dd>ニンテンドー</dd>
  <dt>販売価格：</dt>
  <dd>未定</dd>
  </dl>

  <h2>商品説明</h2>
  <p>2015年7月28日の「ドラゴンクエスト新作発表会」にて正式発表された、ドラゴンクエストシリーズのナンバリングタイトル第11作目。これまでのナンバリングタイトルの多くはニンテンドー用のシングルプラットフォームだったが、今作はシリーズ初の両ハードでのマルチプラットフォームの発売となった。</p>
</body>
</html>