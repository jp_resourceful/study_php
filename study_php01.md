# PHP基礎01

## ようこそPHP

* lesson01/index.php

```php

<?php

echo "hello world";


?>
```

### PHPの機能

* lesson01/index.php

```php
<?php
  echo date("Y/m/d");
  //phpinfo();
?>
```

## 変数と定数

PHPでは、型の宣言が必要なくJavaScriptのようなvarもありません。
最初に`$`をつけて変数を宣言します。

### 変数の定義

* lesson01/index.php

```php
<?php
$hensu;
$hensu = 10;
echo $hensu;

//$hensu = "hello";
//echo $hensu;

?>
```

### 変数の使用例

* lesson01/index.php **(index.phpは基本的に省略可能)**

```php
<?php
//1文字目が英語であること
$hensu=20;

//型の代わりに変数名でわかりやすく説明
$price = 100;
$orderNo= 1;
$messageText= "hello";
$memberName = "shimizu";

//数値でもゼロ詰めが必要なID,コードは文字列型で。
$zipCode = "5320004"

//審議判定にはisxxxxx;
$isCheck =true;

//変数を利用した計算も可能。
$height = 5.25;
$bottom = 2.75;
$result = $height * $bottom/2;

//計算した変数を表示。
echo $result;

//※文字列同士の結合は次章で。

?>
```

### PHPの予約語

```php
'__halt_compiler', 'abstract', 'and', 'array', 'as', 'break', 'callable', 'case', 'catch', 'class', 'clone', 'const', 'continue', 'declare', 'default', 'die', 'do', 'echo', 'else', 'elseif', 'empty', 'enddeclare', 'endfor', 'endforeach', 'endif', 'endswitch', 'endwhile', 'eval', 'exit', 'extends', 'final', 'for', 'foreach', 'function', 'global', 'goto', 'if', 'implements', 'include', 'include_once', 'instanceof', 'insteadof', 'interface', 'isset', 'list', 'namespace', 'new', 'or', 'print', 'private', 'protected', 'public', 'require', 'require_once', 'return', 'static', 'switch', 'throw', 'trait', 'try', 'unset', 'use', 'var', 'while', 'xor','__CLASS__', '__DIR__', '__FILE__', '__FUNCTION__', '__LINE__', '__METHOD__', '__NAMESPACE__', '__TRAIT__';
```

### 定数の定義

* lesson01/index2.php

```PHP
<?php

define("PI",3.141592);
echo PI;

?>
```

### 定義済定数

|定数名|説明|
|:-----:|:---:|
|`__LINE__`|ファイル上の現在の行番号|
|`__FILE__`|ファイルのフルパスとファイル名|
|`__FUNCTION__`|関数名（PHP4.3.0で追加）|
|`__CLASS__`|クラス名（PHP4.3.0で追加）|
|`__METHOD__`|クラスのメソッド名（PHP5.0.0で追加）|

etc…

## 文字列

※以下より`<?PHP …?>`省略してあるので、
前章の続きに記入すること。

### 文字列の展開

* lesson01/index2.php

```php
$name = "Shimizu";

//ダブルクォーテーション時は内部の変数が展開される
echo "My name is $name";

//シングルクォーテーション時は内部の変数が展開される
echo 'My name is $name';
```

#### 以下別パターン

* lesson01/index2.php

```php

$fullName = "kenji Shimizu"
echo "My name is ${fullName}";
echo "My name is {$fullName}";
```

### 文字列の連結

* lesson01/index2.php

```php
$maker = "Nintendo";
$device = "switch";
echo 'this is '.$maker.$device;
```

### 改行文字を連結

* lesson01/index2.php

```php
$maker = "Nintendo";
$device = "switch";
echo 'this is '."\n".$maker."\n".$device;
```

## HTML上でPHPを表示

### HTMLとPHPブロックを分ける

* lesson02/index.php

```html
<?php
  $maker = "SONY";
  $device = "PlayStation4";
?>
<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>PHP Lesson</title>
</head>
<body>
  <h1>Lesson02</h1>
  <p></p>
  <p></p>
</body>
</html>

```

### 演習Q1

* lesson02/q1.php

```html
<?php
$maker;
$device;
$price;
?>

<!DOCTYPE html>
<html lang="ja">
<head>
  <meta charset="UTF-8">
  <title>PHP Lesson</title>
</head>
<body>
  <h1>Q1</h1>
  <h2>ニンテンドースイッチ版ドラゴンクエストXI</h2>

  <dl>
  <dt>発売元：</dt>
  <dd>ニンテンドー</dd>
  <dt>販売価格：</dt>
  <dd>未定</dd>
  </dl>

  <h2>商品説明</h2>
  <p>2015年7月28日の「ドラゴンクエスト新作発表会」にて正式発表された、ドラゴンクエストシリーズのナンバリングタイトル第11作目。これまでのナンバリングタイトルの多くはニンテンドー用のシングルプラットフォームだったが、今作はシリーズ初の両ハードでのマルチプラットフォームの発売となった。</p>
</body>
</html>

```

## 条件分岐処理

### コロン構文

### 反復処理

## 配列

### 配列の定義

### 連想配列の定義

### 配列の読み込み

### 配列の書き出し

## 関数

### 関数の定義

### 関数の使用

## ローカル変数とグローバル変数

## PHPの外部ファイル化

### 外部ファイルの読み込み

### 名前空間(ネームスペース)

### PHPの終了タグ

## 例外処理

## クラス

### クラスの定義

### コンストラクタ

### クラスのオーバーロード

### インスタンスの定義

### クラスメソッドの使用

## クラスの継承

### クラスのオーバーライド

## アクセス権

## クラスの抽象化(ポリモーフィズム)

### インターフェイスの定義

## PHPでファイルを開いて読み込む

https://qiita.com/tadsan/items/bbc23ee596d55159f044

## SplFileObjectでファイルを開いて読み込む

https://qiita.com/suin/items/31a1c7c47f49cb53f2a7

## PHPでJSONファイルを読み込む

https://qiita.com/fantm21/items/603cbabf2e78cb08133e

## PHPでデータベース接続

https://qiita.com/tabo_purify/items/2575a58c54e43cd59630

## PHPとMysqlとUnityの接続

https://www.google.co.jp/search?rlz=1C1GGRV_enJP751JP751&biw=1366&bih=613&ei=vZVPWpXXCcaR8gXH6KXADw&q=unity+php&oq=unity+php&gs_l=psy-ab.3..35i39k1j0l5j0i203k1l2.3125.3446.0.3861.3.3.0.0.0.0.132.357.0j3.3.0....0...1c.1.64.psy-ab..0.3.355...0i4k1.0.tF_8STWBRjU
